<!DOCTYPE html>
<html>
<title>CV</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="master.css">
<link rel='stylesheet' href='<link href="https://fonts.googleapis.com/css?family=Josefin+Slab" rel="stylesheet">'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5,h6 {font-family: "Josefin Slab", sans-serif}
</style>
<body class="w3-light-grey">

<!-- Page Container -->
<div class="w3-content w3-margin-top" style="max-width:1400px;">

  <!-- The Grid -->
  <div class="w3-row-padding">

    <!-- Left Column -->
    <div class="w3-third">

      <div class="w3-white w3-text-grey w3-card-4">
        <div class="w3-display-container">
          <img src="avatar_hat.jpg" style="width:100%" alt="Avatar">
          <div class=".w3-display-bottom w3-container w3-large w3-text-purple">
            <h2 class="w3-center pb-boldness">Francisco Blanco</h2>
            <a href="index.php" title="Español"><h6 class="w3-medium w3-text-deep-purple"><i class="fa fa-language fa-2x fa-fw w3-margin-right"></i><span class="w3-tag w3-purple w3-round">Change Language</span></h6></a>
          </div>
        </div>
        <div class="w3-container">
          <p><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-purple"></i>Developer</p>
          <p><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-purple"></i>Buenos Aires, Ar</p>
          <p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-purple"></i>yo@panchoblanco.com.ar</p>
          <p><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-purple"></i>15-3175-2829</p>
          <hr class="w3-deep-purple">

          <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-purple"></i>Skills</b></p>
          <!-- <p>Adobe Photoshop CC</p> -->
          <!-- <p>Adobe Illustrator CC</p> -->
          <p>Full Stack Web Development <i class="w3-small">HTML 5, CSS 3, Scrum, Bootstrap, PHP, POO, GitHub, MySQL, UML, JavaScript, Laravel</i> </p>
          <p>Adobe Suite CC <i class="w3-small">Photoshop, Illustrator, InDesign, After Effects, Premiere, Animate</i> </p>
          <!-- <p>Adobe InDesign CC</p> -->
          <p>Photography</p>
          <p>Video Editing</p>
          <p>Unity & C#</p>
          <p>Maya </p>
          <!-- <p>Adobe After Effects CC </p> -->
          <p>3DMax </p>
          <!-- <p>Adobe Premiere CC </p> -->
          <p>ZBrush </p>
          <!-- <p>Adobe Animate CC </p> -->
          <hr class="w3-deep-purple">
          <br>
          <p class="w3-large w3-text-theme"><b><i class="fa fa-globe fa-fw w3-margin-right w3-text-purple"></i>Languages</b></p>
          <p>Spanish</p>
          <div class="w3-light-grey w3-round-xlarge">
            <div class="w3-round-xlarge w3-purple" style="height:24px;width:100%">
            <h6 style="text-align: center;" >Mother Tongue</h6></div>
          </div>
          <p>English</p>
          <div class="w3-light-grey w3-round-xlarge">
            <div class="w3-round-xlarge w3-purple" style="height:24px;width:100%">
            <h6 style="text-align: center;" >Native</h6></div>
          </div>
          <p>French</p>
          <div class="w3-light-grey w3-round-xlarge">
            <div class="w3-round-xlarge w3-purple" style="height:24px;width:25%">
            <h6 style="text-align: center;" >Basic</h6></div>
          </div>
          <br>
        </div>
      </div><br>

    <!-- End Left Column -->
    </div>

    <!-- Right Column -->
    <div class="w3-twothird">

      <div class="w3-container w3-card-2 w3-white w3-margin-bottom">
        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-purple"></i>Work Experience</h2>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>Full Stack Developer / Rolify</b></h5>
          <h6 class="w3-text-purple"><i class="fa fa-calendar fa-fw w3-margin-right"></i>Jan 2017 - <span class="w3-tag w3-purple w3-round">Current</span></h6>
          <p>Landing Page development, Overall site design, PHP development, Random functionality, Illustrations.</p>
          <hr class="w3-deep-purple">
        </div>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>Production Design / NetDreams</b></h5>
          <h6 class="w3-text-purple"><i class="fa fa-calendar fa-fw w3-margin-right"></i>Jan 2016 - Jul 2016</h6>
          <p>Photo Editing, Cooperative workflow, Training, Data Management, Interaction with International Clients.</p>
          <hr class="w3-deep-purple">
        </div>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>Jr Art Director / Musculo Creativo</b></h5>
          <h6 class="w3-text-purple"><i class="fa fa-calendar fa-fw w3-margin-right"></i>May 2015 - Jan 2016</h6>
          <p>Logo Design, Digital Marketing, Campaign Development, Identity design, Stand Design, Events.</p>
          <hr class="w3-deep-purple">
        </div>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>Trainee / MindSet</b></h5>
          <h6 class="w3-text-purple"><i class="fa fa-calendar fa-fw w3-margin-right"></i>Jun 2009 - Dic 2009</h6>
          <p>Animation, Link between Artists and Programmers, Design.</p>
        </div>
      </div>

      <div class="w3-container w3-card-2 w3-white">
        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-certificate fa-fw w3-margin-right w3-xxlarge w3-text-purple"></i>Education</h2>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>Digital House</b></h5>
          <h6 class="w3-text-purple"><i class="fa fa-calendar fa-fw w3-margin-right"></i>2017</h6>
          <p>Full Stack Web Development!
            <i>HTML 5, CSS 3, Scrum, Bootstrap, PHP, POO, GitHub, MySQL, UML</i></p>
          <hr class="w3-deep-purple">
        </div>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>UP - Universidad de Palermo</b></h5>
          <h6 class="w3-text-purple"><i class="fa fa-calendar fa-fw w3-margin-right"></i>2009 - 2013</h6>
          <p>Graphic Design</p>
          <hr class="w3-deep-purple">
        </div>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>St Luke's Collage</b></h5>
          <h6 class="w3-text-purple"><i class="fa fa-calendar fa-fw w3-margin-right"></i>1996 - 2008</h6>
          <p>Bachelor Degree (Bilingual) </p><br>
        </div>
      </div>

    <!-- End Right Column -->
    </div>

  <!-- End Grid -->
  </div>

  <!-- End Page Container -->
</div>

<footer class="w3-container w3-purple w3-center w3-margin-top">
  <p>Find me on social media.</p>
  <a href="https://www.facebook.com/PanchoBB" target="_blank" class="w3-margin-right"><i class="fa fa-facebook-official w3-hover-opacity"></i></a>
  <a href="https://www.behance.net/ArBust" target="_blank" class="w3-margin-right"><i class="fa fa-behance w3-hover-opacity"></i></a>
  <a href="http://panhcoblanco.deviantart.com/" target="_blank" class="w3-margin-right"><i class="fa fa-deviantart w3-hover-opacity"></i></a>
  <a href="https://www.linkedin.com/in/franmblanco/" target="_blank"><i class="fa fa-linkedin w3-hover-opacity"></i></a>
  <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
</footer>

</body>
</html>
