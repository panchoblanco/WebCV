<?php

namespace App\Http\Controllers;

use App\Experiencie;
use Illuminate\Http\Request;

class ExperiencieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Experiencie  $experiencie
     * @return \Illuminate\Http\Response
     */
    public function show(Experiencie $experiencie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Experiencie  $experiencie
     * @return \Illuminate\Http\Response
     */
    public function edit(Experiencie $experiencie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Experiencie  $experiencie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Experiencie $experiencie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Experiencie  $experiencie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Experiencie $experiencie)
    {
        //
    }
}
