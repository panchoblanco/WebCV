<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Footer Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used fot the footer of this CV.
    |
     */

    'reach' => 'Find me on social media.',

];
